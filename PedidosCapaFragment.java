package br.com.gestaodepedidos.view;

import br.com.gestaodepedidos.model.FormaPagamento;
import br.com.gestaodepedidos.model.Pedido;
import br.com.gestaodepedidos.model.controller.ClienteController;
import br.com.gestaodepedidos.model.controller.CondicaoPagamentoController;
import br.com.gestaodepedidos.model.controller.PedidoController;
import br.com.gestaodepedidos.model.controller.TabelaPrecoController;
import br.com.gestaodepedidos.model.exception.PedidoException;
import br.com.gestaodepedidos.util.Util;
import br.com.gestaodepedidos.R;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.database.Cursor;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import static br.com.gestaodepedidos.R.color.button_text_color;

public class PedidosCapaFragment extends Fragment implements OnClickListener, OnFocusChangeListener {

	private Context context;
	private PedidoController controller;
	private ClienteController clienteController;
	private TabelaPrecoController tabelaController;
	private CondicaoPagamentoController condicaoController;
    private Pedido pedido;

	private long pedido_id;
    private long cliente_id;

	private Spinner cliente;
	private EditText numeroPedido;
	private Spinner tabelaPreco;
	private EditText emissao;
    private EditText entrega;
	private Spinner formaPagamento;
	private Spinner condicaoPagamento;
	private EditText numeroPedidoCliente;
	private EditText remessa;
    private Spinner tipoPedido;
    private Spinner enviarEmail;
	private EditText observacoes;
    private String latitude;
    private String longitude;

	private Button gravar;
    private Button pesquisacliente;
	private String mensagem;

	public PedidosCapaFragment(){
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_pedidos_capa, container, false);
		return rootView;

	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		this.CarregaDados(view);
	}

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.Gravar:
			Gravar();
			break;
        case R.id.PesquisaCliente:
            PesquisaCliente();
            break;
		}



	}

	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		if (v.getId() == R.id.PedidoDataEmissao) {
			if (hasFocus) {
				Util.DatePicker(context, emissao);
			}
		}else if (v.getId() == R.id.PedidoDataEntrega) {
            if (hasFocus) {
                Util.DatePicker(context, entrega);
            }
        }
	}
	
	private void CarregaDados(View view) {
		
		Bundle args = getArguments();
		pedido_id = args.getLong("Pedido_id", 0);
        cliente_id = args.getLong("Cliente_id", 0);

		context = view.getContext();
		controller = new PedidoController(context);
		clienteController = new ClienteController(context);
		tabelaController = new TabelaPrecoController(context);
		condicaoController = new CondicaoPagamentoController(context);

        pedido = controller.BuscarPor_id(pedido_id);

		cliente = (Spinner) view.findViewById(R.id.PedidoCliente_ID);
		numeroPedido = (EditText) view.findViewById(R.id.PedidoNumeroPedidoRepresentante);
		numeroPedido.setSelected(true);
        numeroPedido.setEnabled(false);
		tabelaPreco = (Spinner) view.findViewById(R.id.PedidoTabelaPrecoID);
		emissao = (EditText) view.findViewById(R.id.PedidoDataEmissao);
		emissao.setInputType(InputType.TYPE_NULL);
		emissao.setOnFocusChangeListener(this);
        emissao.setEnabled(false);
        entrega = (EditText) view.findViewById(R.id.PedidoDataEntrega);
        entrega.setInputType(InputType.TYPE_NULL);
        entrega.setOnFocusChangeListener(this);

		formaPagamento = (Spinner) view.findViewById(R.id.PedidoFormaPagamento);
		condicaoPagamento = (Spinner) view.findViewById(R.id.PedidoCondicaoPagamentoID);
		numeroPedidoCliente = (EditText) view.findViewById(R.id.PedidoNumeroPedidoCliente);
		remessa = (EditText) view.findViewById(R.id.PedidoRemessa);
		tipoPedido = (Spinner) view.findViewById(R.id.PedidoTipoPedido);
        enviarEmail = (Spinner) view.findViewById(R.id.PedidoEnviarEmail);
        observacoes = (EditText) view.findViewById(R.id.PedidoObservacoes);

		gravar = (Button) view.findViewById(R.id.Gravar);
		gravar.setOnClickListener(this);

        pesquisacliente = (Button) view.findViewById(R.id.PesquisaCliente);
        pesquisacliente.setOnClickListener(this);

		ListarPedido();

    }

	private void ListarPedido() {

        int i = 0;

        emissao.setText(Util.DataAtual());
        entrega.setText(Util.DataAtual());
        entrega.setText(Util.DataAtual());
        remessa.setText("1");
        numeroPedido.setText(Util.ConverteString(controller.ProximoNumeroPedido(Util.RepresentanteLogado(context))));

		ArrayAdapter arrayForma = new ArrayAdapter(context,android.R.layout.simple_spinner_item, FormaPagamento.Listar().toArray());
        arrayForma.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        formaPagamento.setAdapter(arrayForma);

        ArrayAdapter arrayTipoPedido = new ArrayAdapter(context, android.R.layout.simple_spinner_item, Pedido.ListarTipoPedido().toArray());
        arrayTipoPedido.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        tipoPedido.setAdapter(arrayTipoPedido);

        ArrayAdapter arrayEnviarEmail = new ArrayAdapter(context, android.R.layout.simple_spinner_item, Pedido.ListarSimNao().toArray());
        arrayEnviarEmail.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        enviarEmail.setAdapter(arrayEnviarEmail);

		SimpleCursorAdapter clienteAdapter = new SimpleCursorAdapter(context,
				android.R.layout.simple_spinner_item,
				clienteController.Listar(true, Util.RepresentanteLogado(context)), new String[] { "RazaoSocial" },
				new int[] { android.R.id.text1 });

		clienteAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		cliente.setAdapter(clienteAdapter);

        /*se veio da edicao de clientes, seta o cliente selecionado*/
        for (i = 0; i < cliente.getCount(); i++) {
            Cursor value = (Cursor) cliente.getItemAtPosition(i);
            long id = value.getLong(value.getColumnIndex("_id"));
            if (id == cliente_id) {
                cliente.setSelection(i);
            }
        }




        SimpleCursorAdapter condicaoAdapter = new SimpleCursorAdapter(context,
                android.R.layout.simple_spinner_item,
                condicaoController.Listar(), new String[] { "Nome" },
                new int[] { android.R.id.text1 });

        condicaoAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        condicaoPagamento.setAdapter(condicaoAdapter);

        cliente.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                    cliente_id = cliente.getSelectedItemId();
                    if (cliente_id != 0) {
                        int i = 0;
                        long CpSetado = 0;
                        Cursor teste = clienteController.Listar(cliente_id);

                        if (teste.moveToFirst()) {
                            long CondicaoPagamentoId = teste.getLong(teste.getColumnIndex("CondicaoPagamentoID"));

                            for (i = 0; i < condicaoPagamento.getCount(); i++) {
                                Cursor value = (Cursor) condicaoPagamento.getItemAtPosition(i);
                                long id = value.getLong(value.getColumnIndex("ID"));
                                if (id == CondicaoPagamentoId) {
                                    condicaoPagamento.setSelection(i);
                                    CpSetado = CondicaoPagamentoId;
                                }
                            }
                            if (CondicaoPagamentoId > 0) {
                                SimpleCursorAdapter condicaoAdapter = new SimpleCursorAdapter(context,
                                        android.R.layout.simple_spinner_item,
                                        condicaoController.ListarPorPrazoMedio(CondicaoPagamentoId), new String[]{"Nome"},
                                        new int[]{android.R.id.text1});

                                condicaoAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                condicaoPagamento.setAdapter(condicaoAdapter);
                            }else {
                                SimpleCursorAdapter condicaoAdapter = new SimpleCursorAdapter(context,
                                        android.R.layout.simple_spinner_item,
                                        condicaoController.Listar(), new String[] { "Nome" },
                                        new int[] { android.R.id.text1 });
                                condicaoAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                condicaoPagamento.setAdapter(condicaoAdapter);
                            }
                        }
                    }
                }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });



        SimpleCursorAdapter tabelaAdapter = new SimpleCursorAdapter(context,
				android.R.layout.simple_spinner_item,
				tabelaController.Listar(), new String[] { "Nome" },
				new int[] { android.R.id.text1 });

		tabelaAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		tabelaPreco.setAdapter(tabelaAdapter);



		Cursor cursor = this.controller.Listar(pedido_id);

        if (cursor != null && cursor.moveToFirst()) {

            cliente_id = cursor.getLong(cursor.getColumnIndex("Cliente_ID"));
            if (cliente_id == 0){
                cliente_id  = clienteController.BuscarPorIdExterno(cursor.getLong(cursor.getColumnIndex("ClienteEnderecoID"))).get_id();
            }

            for (i = 0; i < cliente.getCount(); i++) {
			    Cursor value = (Cursor) cliente.getItemAtPosition(i);
			    long id = value.getLong(value.getColumnIndex("_id"));
			    if (id == cliente_id) {
			    	cliente.setSelection(i);  
			    }  
			}
            numeroPedido.setText(cursor.getString(cursor.getColumnIndex("NumeroPedidoRepresentante")));
			for (i = 0; i < tabelaPreco.getCount(); i++) {  
			    Cursor value = (Cursor) tabelaPreco.getItemAtPosition(i);  
			    long id = value.getLong(value.getColumnIndex("ID"));  
			    if (id == cursor.getLong(cursor.getColumnIndex("TabelaPrecoID"))) {  
			    	tabelaPreco.setSelection(i);  
			    }  
			}
            emissao.setText(cursor.getString(cursor.getColumnIndex("DataEmissao")));
            entrega.setText(cursor.getString(cursor.getColumnIndex("DataEntrega")));
            formaPagamento.setSelection(FormaPagamento.RetornaForma(cursor.getString(cursor.getColumnIndex("FormaPagamento"))));
            for (i = 0; i < condicaoPagamento.getCount(); i++) {
                Cursor value = (Cursor) condicaoPagamento.getItemAtPosition(i);
                long id = value.getLong(value.getColumnIndex("ID"));
                if (id == cursor.getLong(cursor.getColumnIndex("CondicaoPagamentoID"))) {
                    condicaoPagamento.setSelection(i);
                }
            }
            numeroPedidoCliente.setText(cursor.getString(cursor.getColumnIndex("NumeroPedidoCliente")));
            remessa.setText(cursor.getString(cursor.getColumnIndex("Remessa")));
            tipoPedido.setSelection((cursor.getString(cursor.getColumnIndex("TipoPedido")).toString().equals("V") ? 0 : 1));

            if (cursor.getString(cursor.getColumnIndex("EnviarEmail")) != null){
                enviarEmail.setSelection((cursor.getString(cursor.getColumnIndex("EnviarEmail")).toString().equals("S") ? 0 : 1 ));
            }else{
                enviarEmail.setSelection(1);
            }

            observacoes.setText(cursor.getString(cursor.getColumnIndex("Observacoes")));

            if (cursor.getLong(cursor.getColumnIndex("ID")) > 0 ){
                gravar.setEnabled(false);
                gravar.setBackgroundColor(getResources().getColor(button_text_color));
            }

            cliente.setEnabled(false);

            cursor.close();
		}

	}

    private boolean ValidaCampos(){

        boolean retorno = true;
        mensagem =  "";

        if (emissao.getText().toString().equals("")){
            mensagem = context.getString(R.string.pedidos_erro_emissao);
            retorno = false;
            emissao.setFocusable(true);
        }

        if (entrega.getText().toString().equals("")){
            mensagem = context.getString(R.string.pedidos_erro_entrega);
            retorno = false;
            entrega.setFocusable(true);
        }

        if (numeroPedido.getText().toString().equals("")){
            mensagem = context.getString(R.string.pedidos_erro_numero);
            retorno = false;
            numeroPedido.setFocusable(true);
        }

        if (cliente.getCount() == 0 || cliente.getSelectedItemPosition() == 0){
            mensagem = context.getString(R.string.pedidos_erro_cliente);
            retorno = false;
            cliente.setFocusable(true);
        }

        if (!controller.ValidaNumeroPedido(pedido_id, numeroPedido.getText().toString())){
            mensagem = context.getString(R.string.pedidos_erro_numero_pedido);
            retorno = false;
            cliente.setFocusable(true);
        }

        if (cliente.getCount() > 0) {
            Cursor Value = (Cursor) cliente.getItemAtPosition(cliente.getSelectedItemPosition());

            if (!controller.ValidaRemessa(pedido_id, Value.getLong(Value.getColumnIndex("_id")), entrega.getText().toString(), Util.ConverteInt(remessa.getText().toString()))){
                mensagem = context.getString(R.string.pedidos_erro_remessa);
                retorno = false;
                remessa.setFocusable(true);
            }
        }

        if (retorno == false){
            Toast.makeText(context, mensagem, Toast.LENGTH_LONG).show();
        }

        return retorno;

    }

	private void Gravar() {

        if (ValidaCampos()) {

            pedido = controller.BuscarPor_id(pedido_id);

            if (pedido == null) {
                pedido = new Pedido();
                pedido.setUnidadeEmpresaID(Util.ConverteLong(Util.SessaoAtiva(context).getString("unidadeEmpresaID", "0")));
                pedido.setRepresentanteID(Util.ConverteLong(Util.SessaoAtiva(context).getString("representanteID", "0")));
                pedido.setEnviarEmail("N");
            }

            pedido.set_id(pedido_id);

            if (cliente.getCount() > 0) {
                Cursor Value = (Cursor) cliente.getItemAtPosition(cliente.getSelectedItemPosition());
                pedido.setCliente_ID(Value.getLong(Value.getColumnIndex("_id")));
                pedido.setClienteEnderecoID(Value.getLong(Value.getColumnIndex("ClienteEnderecoID")));
            }

            pedido.setNumeroPedidoRepresentante(numeroPedido.getText().toString());
            pedido.setDataEmissao(emissao.getText().toString());
            pedido.setDataEntrega(entrega.getText().toString());
            pedido.setRemessa(Util.ConverteInt(remessa.getText().toString()));

            if (tabelaPreco.getCount() > 0) {
                Cursor Value = (Cursor) tabelaPreco.getItemAtPosition(tabelaPreco.getSelectedItemPosition());
                pedido.setTabelaPrecoID(Value.getLong(Value.getColumnIndex("ID")));
            }
            if (condicaoPagamento.getCount() > 0) {
                Cursor Value = (Cursor) condicaoPagamento.getItemAtPosition(condicaoPagamento.getSelectedItemPosition());
                pedido.setCondicaoPagamentoID(Value.getLong(Value.getColumnIndex("ID")));
            }

            if (formaPagamento.getCount() > 0) {
                pedido.setFormaPagamento(formaPagamento.getSelectedItem().toString());
            }

            pedido.setNumeroPedidoCliente(numeroPedidoCliente.getText().toString());

            if (tipoPedido.getCount() > 0) {
                pedido.setTipoPedido((tipoPedido.getSelectedItem().toString() == "VENDA" ? "V" : "B"));
            }

            if (enviarEmail.getCount() > 0) {
                pedido.setEnviarEmail((enviarEmail.getSelectedItem().toString() == "SIM" ? "S" : "N"));
            }

            pedido.setStatus("D");
            pedido.setSituacaoPedido("EM DIGITAÇÃO");
            pedido.setObservacoes(observacoes.getText().toString());
            pedido.setLatitude(latitude);
            pedido.setLongitude(longitude);

            try {
                pedido_id = this.controller.Persistir(pedido);
                mensagem = "Pedido gravado com sucesso!";
                ((PedidosActivity) getActivity()).set_pedidoId(pedido_id);
                PedidosItens();
            } catch (PedidoException e) {
                e.printStackTrace();
                mensagem = e.getMessage();
            } finally {
                Toast.makeText(context, mensagem, Toast.LENGTH_LONG).show();
            }
        }
	}

    public void PesquisaCliente(){
        Fragment newFragment = new ClientesFragment();

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
	public void PedidosItens() {
        ((PedidosActivity)getActivity()).Itens();
	}

}

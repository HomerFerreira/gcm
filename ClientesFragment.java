package br.com.gestaodepedidos.view;

import br.com.gestaodepedidos.R;
import br.com.gestaodepedidos.model.controller.ClienteController;
import br.com.gestaodepedidos.util.Util;
import br.com.gestaodepedidos.view.adapter.ClientesCursorAdapter;

import android.app.Fragment;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class ClientesFragment extends Fragment implements OnClickListener{
	
	private Context context;
	private ClienteController controller;
	private Cursor cursor;
	
	private ListView listaClientes;

	private EditText id;
	private EditText razaoSocial;
	private EditText cnpj;
	private EditText cidade;
	private Button adicionar;
	private Button localizar;
	
	public ClientesFragment(){
		
	}

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		View rootView = inflater.inflate(R.layout.fragment_clientes, container, false);
		
		return rootView;
		
    }

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		this.CarregaDados(view);
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.Localizar) {
			Localizar();
		}else if (v.getId() == R.id.Adicionar) {
			Editar((long) 0);
		}
	}
	
	private void CarregaDados(View view) {
		
		this.context = view.getContext();
		this.controller = new ClienteController(context);
		this.listaClientes = (ListView) view.findViewById(R.id.ClientesListView);
		
		id = (EditText) view.findViewById(R.id.LocalizarID);
		razaoSocial = (EditText) view.findViewById(R.id.LocalizarNome);
		cnpj = (EditText) view.findViewById(R.id.LocalizarCNPJ);
		cidade = (EditText) view.findViewById(R.id.LocalizarCidade);
		
		adicionar = (Button) view.findViewById(R.id.Adicionar);
		adicionar.setOnClickListener(this);	

		localizar = (Button) view.findViewById(R.id.Localizar);
		localizar.setOnClickListener(this);	
		
		this.ListarClientes();
		
	}

	private void ListarClientes() {
		
		cursor = this.controller.ListarTodos(Util.RepresentanteLogado(context));
		
		if (!id.getText().toString().equals("") || !razaoSocial.getText().toString().equals("") || !cnpj.getText().toString().equals("") || !cidade.getText().toString().equals("")) {
			cursor = this.controller.Listar(Util.ConverteLong(id.getText().toString()), Util.RepresentanteLogado(context), razaoSocial.getText().toString(), cnpj.getText().toString(), cidade.getText().toString());
		}
		
		final ClientesCursorAdapter adapter = new ClientesCursorAdapter(context, cursor);
		this.listaClientes.setAdapter(adapter);
		this.listaClientes.setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
				//Toast.makeText(context, "Voce clicou no planeta " +  id, Toast.LENGTH_LONG).show();
				Editar(id);
				return true;
			}
		});	
		
		if (cursor.getCount() == 0) {
			Toast.makeText(context, R.string.mensagem_sem_registros, Toast.LENGTH_SHORT).show();
		}
		
	}	
	
	private void Localizar() {
		ListarClientes();
	}

	private void Editar(long _id) {
	    /*Fragment newFragment = new ClientesCadastroFragment();
        Bundle args = new Bundle();
        args.putLong("_id", _id);

        newFragment.setArguments(args);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
	    transaction.replace(R.id.frame_container, newFragment);
	    transaction.addToBackStack(null);
	    transaction.commit(); */
	}

}



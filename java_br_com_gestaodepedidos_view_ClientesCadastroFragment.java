package br.com.gestaodepedidos.view;

import br.com.gestaodepedidos.model.Cidade;
import br.com.gestaodepedidos.model.Cliente;
import br.com.gestaodepedidos.model.controller.CidadeController;
import br.com.gestaodepedidos.model.controller.ClienteController;
import br.com.gestaodepedidos.model.controller.SegmentoMercadoController;
import br.com.gestaodepedidos.model.exception.PedidoException;
import br.com.gestaodepedidos.util.Util;
import br.com.gestaodepedidos.R;


import android.content.Intent;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.Toast;

public class ClientesCadastroFragment extends Fragment implements OnClickListener{

	private Context context;
	private ClienteController controller;
	private CidadeController cidadeController;
	private SegmentoMercadoController segmentoController;
	
	private long _id = (long) 0;

	private EditText id;
	private EditText nome;
	private EditText fantasia;
	private EditText cnpj;
	private EditText inscricao;
	private Spinner estado;
	private Spinner cidade;
	private RadioGroup isento;
	private EditText filial;
	private EditText limitecredito;
	private EditText limiteutilizado;
	private Spinner segmento;
	private EditText endereco;
	private EditText bairro;
	private EditText complemento;
	private EditText numero;
	private EditText cep;
	private EditText telefone;
	private EditText celular;
	private EditText fax;
	private EditText contato;
	private EditText site;
	private EditText email;
	private EditText observacoes;

	private TextWatcher cpfMask;
	private TextWatcher cnpjMask;
    private TextWatcher telefoneMask;
    private TextWatcher celularMask;
	private RadioGroup radioGroup;
	private RadioButton rdCNPJ;
	private RadioButton naoIsento;
	private RadioButton simIsento;


	private Button gravar;
	private RadioButton fisica;
	private RadioButton juridica;
	private String mensagem;

	public ClientesCadastroFragment(){

	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
	 	View rootView = inflater.inflate(R.layout.fragment_clientes_cadastro, container, false);
	 	return rootView;
		
    }

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		this.CarregaDados(view);
	}

	@Override
	public void onClick(View v) {

	    switch(v.getId()) {
	        case R.id.Gravar:
	        	Gravar();
	        	break;
	        case R.id.cliente_tipo_fisica:
	            if (((RadioButton) v).isChecked()) {
					cnpj.addTextChangedListener(Util.insert(Util.CPF_MASK, cnpj));
				}
	            break;
	        case R.id.cliente_tipo_juridica:
	            if (((RadioButton) v).isChecked()) {
					cnpj.addTextChangedListener(Util.insert(Util.CPF_MASK, cnpj));
	            }
	            break;
	    }
		
	}	
	
	private void CarregaDados(View view) {
        Bundle args = getArguments();
        _id = args.getLong("_id", 0);

		context = view.getContext();
		controller = new ClienteController(context);
		cidadeController = new CidadeController(context);
		segmentoController = new SegmentoMercadoController(context);
		
		id = (EditText) view.findViewById(R.id.CadastroID);
		nome = (EditText) view.findViewById(R.id.CadastroNome);
		fantasia = (EditText) view.findViewById(R.id.CadastroNomeFantasia);
		cnpj = (EditText) view.findViewById(R.id.CadastroCNPJ);

		filial = (EditText) view.findViewById(R.id.Filial);
		isento = (RadioGroup) view.findViewById(R.id.Isento);
		limitecredito = (EditText) view.findViewById(R.id.LimiteCredito);
		limiteutilizado = (EditText) view.findViewById(R.id.LimiteUtilizado);
		naoIsento = (RadioButton) view.findViewById(R.id.cliente_nao_isento);
		simIsento = (RadioButton) view.findViewById(R.id.cliente_sim_isento);

		final EditText cpf = (EditText) view.findViewById(R.id.CadastroCNPJ);
		cpfMask = Util.insert("###.###.###-##", cpf);
        cnpjMask = Util.insert("##.###.###/####-##", cpf);
        cpf.addTextChangedListener(cnpjMask);

		rdCNPJ = (RadioButton) view.findViewById(R.id.cliente_tipo_juridica);

		radioGroup = (RadioGroup) view.findViewById(R.id.radioGroup1);
		radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				int opcao = radioGroup.getCheckedRadioButtonId();
				if (opcao == rdCNPJ.getId()) {
					cpf.removeTextChangedListener(cpfMask);
					cpf.removeTextChangedListener(cnpjMask);
					cpf.addTextChangedListener(cnpjMask);
					cpf.setText("");
					cnpj.requestFocus();
				} else {
					cpf.removeTextChangedListener(cnpjMask);
					cpf.removeTextChangedListener(cpfMask);
					cpf.addTextChangedListener(cpfMask);
					cpf.setText("");
					cnpj.requestFocus();
				}
			}
		});

		inscricao = (EditText) view.findViewById(R.id.CadastroInscricao);
		estado = (Spinner) view.findViewById(R.id.CadastroEstado);
		cidade = (Spinner) view.findViewById(R.id.CadastroCidade);
		segmento = (Spinner) view.findViewById(R.id.CadastroSegmento);
		endereco = (EditText) view.findViewById(R.id.CadastroEndereco);
		bairro = (EditText) view.findViewById(R.id.CadastroBairro);
		complemento = (EditText) view.findViewById(R.id.CadastroComplemento);
		numero = (EditText) view.findViewById(R.id.CadastroNumero);
		cep = (EditText) view.findViewById(R.id.CadastroCep);

        telefone = (EditText) view.findViewById(R.id.CadastroTelefone);
        final EditText fone = (EditText) view.findViewById(R.id.CadastroTelefone);
        telefoneMask = Util.insert("(##)####-#####", fone);
        fone.addTextChangedListener(telefoneMask);

        celular = (EditText) view.findViewById(R.id.CadastroCelular);
        final EditText cel = (EditText) view.findViewById(R.id.CadastroCelular);
        celularMask = Util.insert("(##)####-#####", cel);
        cel.addTextChangedListener(celularMask);

		fax = (EditText) view.findViewById(R.id.CadastroFax);
		contato = (EditText) view.findViewById(R.id.CadastroContato);
		site = (EditText) view.findViewById(R.id.CadastroSite);
		email = (EditText) view.findViewById(R.id.CadastroEmail);
		observacoes = (EditText) view.findViewById(R.id.CadastroObservacoes);

		gravar = (Button) view.findViewById(R.id.Gravar);
	 	gravar.setOnClickListener(this);
	 	
	 	fisica = (RadioButton) view.findViewById(R.id.cliente_tipo_fisica);
	 	juridica = (RadioButton) view.findViewById(R.id.cliente_tipo_juridica);

	 	estado.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
	        @Override
	        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) 
	        {                
	        	CarregaCidade(estado.getSelectedItem().toString());
	        }

	        @Override
	        public void onNothingSelected(AdapterView<?> parent) {
	        }
	    });  
	 	
	 	this.ListarCliente();
	 	
	 	
	}
	
	private void CarregaCidade(String estado) {
		Cursor value = null;
		long id = 0;
		if (cidade.getCount() > 0)
		{
			value = (Cursor) cidade.getItemAtPosition(cidade.getSelectedItemPosition());  
			id = value.getLong(value.getColumnIndex("ID"));

		}

		SimpleCursorAdapter cidadeAdapter = new SimpleCursorAdapter(
		        context, android.R.layout.simple_spinner_item,
		        cidadeController.Listar(Util.ConverteLong("0"),estado,""), new String[] { "Nome" },
		        new int[] { android.R.id.text1 });
		 
		cidadeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		cidade.setAdapter(cidadeAdapter);
		
		if (value != null )
		{
			for(int i=0; i < cidade.getCount(); i++) 
			{
				Cursor valueNovo = (Cursor) cidade.getItemAtPosition(i);  
				long idNovo = valueNovo.getLong(value.getColumnIndex("ID"));
				if (id == idNovo) {  
					  this.cidade.setSelection(i);
			  }
			}
		}
	}
	
	private void ListarCliente() {
		int i = 0;
		
		ArrayAdapter arrayEstado = new ArrayAdapter(context,android.R.layout.simple_spinner_item, cidadeController.ListarEstadosString().toArray());
		arrayEstado.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		estado.setAdapter(arrayEstado);

		SimpleCursorAdapter segmentoAdapter = new SimpleCursorAdapter(
		        context, android.R.layout.simple_spinner_item,
		        segmentoController.Listar(), new String[] { "Nome" },
		        new int[] { android.R.id.text1 });
		 
		segmentoAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		segmento.setAdapter(segmentoAdapter);

		
		Cursor cursor = this.controller.Listar(_id); 

		Cliente cliente = this.controller.BuscarPor_id(_id);

		if(cursor != null && cursor.moveToFirst()){
            if (cursor.getString(cursor.getColumnIndex("TpPessoa")).equals("J")){
                juridica.setChecked(true);
            }else{
                fisica.setChecked(true);
            }

			if (cursor.getString(cursor.getColumnIndex("Isento")).equals(0)){
				naoIsento.setChecked(true);
			}else{
				simIsento.setChecked(true);
			}
			this.id.setText(cursor.getString(cursor.getColumnIndex("ID")));
			this.nome.setText(cursor.getString(cursor.getColumnIndex("RazaoSocial")));
			this.fantasia.setText(cursor.getString(cursor.getColumnIndex("Fantasia")));
			this.cnpj.setText(cursor.getString(cursor.getColumnIndex("CNPJ")));
			this.inscricao.setText(cursor.getString(cursor.getColumnIndex("Inscricao")));
			this.endereco.setText(cursor.getString(cursor.getColumnIndex("Endereco")));
			this.bairro.setText(cursor.getString(cursor.getColumnIndex("Bairro")));
			this.complemento.setText(cursor.getString(cursor.getColumnIndex("Complemento")));
			this.numero.setText(cursor.getString(cursor.getColumnIndex("Numero")));
			this.cep.setText(cursor.getString(cursor.getColumnIndex("Cep")));
            if (!cursor.getString(cursor.getColumnIndex("Telefone")).equals("")) {
                this.telefone.setText(cursor.getString(cursor.getColumnIndex("Telefone")));
            }
            if (!cursor.getString(cursor.getColumnIndex("Celular")).equals("")) {
                this.celular.setText(cursor.getString(cursor.getColumnIndex("Celular")));
            }
			this.fax.setText(cursor.getString(cursor.getColumnIndex("Fax")));
			this.contato.setText(cursor.getString(cursor.getColumnIndex("Contato")));	
			this.site.setText(cursor.getString(cursor.getColumnIndex("Site")));
			this.email.setText(cursor.getString(cursor.getColumnIndex("Email")));
			this.observacoes.setText(cursor.getString(cursor.getColumnIndex("Observacoes")));
			this.filial.setText(cursor.getString(cursor.getColumnIndex("Observacoes")));
			this.limiteutilizado.setText(Util.ConverteString(cursor.getColumnIndex("LimiteUtilizado")));
			this.limitecredito.setText(Util.ConverteString(cursor.getColumnIndex("LimiteCredito")));
			
			Cidade cidadeLocalizada = cidadeController.BuscarPorCidadeId(cursor.getLong(cursor.getColumnIndex("CidadeID")));
			
			for(i=0; i < estado.getCount(); i++) {
			  if(cidadeLocalizada.getUF().trim().equals(estado.getItemAtPosition(i))){
				  this.estado.setSelection(i);
				  break;
			  }
			}
			
			CarregaCidade(cidadeLocalizada.getUF());
			
			for(i=0; i < cidade.getCount(); i++) {
				Cursor value = (Cursor) cidade.getItemAtPosition(i);  
				long id = value.getLong(value.getColumnIndex("ID"));
				if (id == cursor.getLong(cursor.getColumnIndex("CidadeID"))) {
					  this.cidade.setSelection(i);
			  }
			}

			for (i = 0; i < segmento.getCount(); i++) {  
			    Cursor value = (Cursor) segmento.getItemAtPosition(i);  
			    long id = value.getLong(value.getColumnIndex("ID"));  
			    if (id == cursor.getLong(cursor.getColumnIndex("SegmentoID"))) {  
			    	segmento.setSelection(i);  
			    }  
			}


			if (!cursor.getString(cursor.getColumnIndex("ID")).equals("0")){
				this.cnpj.setEnabled(false);
				this.inscricao.setEnabled(false);
			}
			
			cursor.close(); 
		}		

	}

    private boolean ValidaCampos() {

        boolean retorno = true;
        mensagem = "";

        if (email.getText().toString().equals("")){
            mensagem = context.getString(R.string.cliente_erro_email);
            retorno = false;
            email.requestFocus();
        }


        if (contato.getText().toString().equals("")){
            mensagem = context.getString(R.string.cliente_erro_contato);
            retorno = false;
            contato.requestFocus();
        }

        if (telefone.getText().toString().equals("")){
            mensagem = context.getString(R.string.cliente_erro_telefone);
            retorno = false;
            telefone.requestFocus();
        }

        if (numero.getText().toString().equals("")){
            mensagem = context.getString(R.string.cliente_erro_numero);
            retorno = false;
            numero.requestFocus();
        }

        if (bairro.getText().toString().equals("")){
            mensagem = context.getString(R.string.cliente_erro_bairro);
            retorno = false;
            bairro.requestFocus();
        }

        if (endereco.getText().toString().equals("")){
            mensagem = context.getString(R.string.cliente_erro_endereco);
            retorno = false;
            endereco.requestFocus();
        }

        if (cep.getText().toString().equals("")){
            mensagem = context.getString(R.string.cliente_erro_cep);
            retorno = false;
            cep.requestFocus();
        }

        if (fantasia.getText().toString().equals("")){
            mensagem = context.getString(R.string.cliente_erro_fantasia);
            retorno = false;
            fantasia.requestFocus();
        }

        if (nome.getText().toString().equals("")){
            mensagem = context.getString(R.string.cliente_erro_razao);
            retorno = false;
            nome.requestFocus();
        }
        if (fisica.isChecked() && !Util.ValidaCPF(Util.RemoveCaracteresNaoNumericos(cnpj.getText().toString()))){
            mensagem = context.getString(R.string.cliente_erro_cpf);
            retorno = false;
            cnpj.requestFocus();
        }

        if (juridica.isChecked() && !Util.ValidaCNPJ(Util.RemoveCaracteresNaoNumericos(cnpj.getText().toString()))){
            mensagem = context.getString(R.string.cliente_erro_cnpj);
            retorno = false;
            cnpj.requestFocus();
        }

        if (retorno == false){
            Toast.makeText(context, mensagem, Toast.LENGTH_SHORT).show();
        }

        return retorno;
    }

	private void Gravar() {

		if (ValidaCampos()){

			Cliente cliente = controller.BuscarPor_id(_id);

			if (cliente == null)
			{
				cliente = new Cliente();
				cliente.setUnidadeEmpresaID(Util.ConverteLong(Util.SessaoAtiva(context).getString("unidadeEmpresaID", "0")));
				cliente.setRepresentanteID(Util.ConverteLong(Util.SessaoAtiva(context).getString("representanteID", "0")));
			}
			cliente.set_id(_id);
			cliente.setID(Util.ConverteLong(id.getText().toString().trim()));
			cliente.setNome(nome.getText().toString().trim());
			cliente.setFantasia(fantasia.getText().toString().trim());
			cliente.setCnpj(cnpj.getText().toString());
			cliente.setTpPessoa((fisica.isChecked() ? "F" : "J"));
			cliente.setInscricao(inscricao.getText().toString().trim());
			cliente.setEndereco(endereco.getText().toString().trim());
			cliente.setBairro(bairro.getText().toString().trim());
			cliente.setComplemento(complemento.getText().toString().trim());
			cliente.setNumero(Util.ConverteLong(numero.getText().toString().trim()));
			cliente.setCep(cep.getText().toString().trim());
			cliente.setTelefone(telefone.getText().toString().trim());
			cliente.setCelular(celular.getText().toString().	trim());
			cliente.setFax(fax.getText().toString().trim());
			cliente.setContato(contato.getText().toString().trim());
			cliente.setSite(site.getText().toString().trim());
			cliente.setEmail(email.getText().toString().trim());
			cliente.setObservacoes(observacoes.getText().toString().trim());
			cliente.setIsento((naoIsento.isChecked() ? 0 : 1));
			cliente.setObservacoes(filial.getText().toString().trim());
			cliente.setLimiteCredito(limitecredito.getText().toString().trim());
			cliente.setLimiteUtilizado(limiteutilizado.getText().toString().trim());

			if (cidade.getCount() > 0)
			{
				Cursor cidadeValue = (Cursor) cidade.getItemAtPosition(cidade.getSelectedItemPosition());
				cliente.setCidadeID(cidadeValue.getLong(cidadeValue.getColumnIndex("ID")));
			}

			if (segmento.getCount() > 0)
			{
				Cursor segmentoValue = (Cursor) segmento.getItemAtPosition(segmento.getSelectedItemPosition());
				cliente.setSegmentoID(segmentoValue.getLong(segmentoValue.getColumnIndex("ID")));
			}
			cliente.setStatus("A");
			try {
				_id = this.controller.Persistir(cliente);
				mensagem = "Cadastro gravado com sucesso!";
				this.Pedido();
			} catch (PedidoException e) {
				e.printStackTrace();
				mensagem = e.getMessage();
			} finally {
				Toast.makeText(context, mensagem, Toast.LENGTH_LONG).show();
			}

		}

	}

    public void Pedido(){

        Intent intent = new Intent(getActivity(), PedidosActivity.class);
        intent.putExtra("Cliente_id", _id);

        getActivity().startActivity(intent);

    }

}

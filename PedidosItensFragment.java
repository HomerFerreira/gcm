package br.com.gestaodepedidos.view;

import java.util.ArrayList;

import br.com.gestaodepedidos.model.PedidoItem;
import br.com.gestaodepedidos.model.Produto;
import br.com.gestaodepedidos.model.TabelaPreco;
import br.com.gestaodepedidos.model.controller.CondicaoPagamentoController;
import br.com.gestaodepedidos.model.controller.PedidoController;
import br.com.gestaodepedidos.model.controller.PedidoItemController;
import br.com.gestaodepedidos.model.controller.ProdutoController;
import br.com.gestaodepedidos.model.controller.TabelaPrecoController;
import br.com.gestaodepedidos.model.exception.PedidoException;
import br.com.gestaodepedidos.util.Util;
import br.com.gestaodepedidos.R;
import br.com.gestaodepedidos.view.adapter.TabelaCursorAdapter;


import android.app.Fragment;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import static br.com.gestaodepedidos.R.color.button_text_color;

public class PedidosItensFragment extends Fragment implements OnClickListener,
		OnItemClickListener, View.OnFocusChangeListener {

	private Context context;
	private PedidoItemController controller;
	private PedidoController pedidoController;
	private ProdutoController produtoController;
	private TabelaPrecoController tabelaController;
	private CondicaoPagamentoController condicaoController;
    private Cursor cursorProduto;
    private Produto produto;
    private PedidoItem pedidoItem;
    private TabelaPreco tabela;

	private long pedido_id;
	private long tabelaPrecoID;
	private long condicaoPagamentoID;
    private long produtoID;
    private String nome;

    private float quantidade;
    private float fracao;
    private float precoTabela;
    private float desconto;
    private float preco;
    private float dcaPermitido;
    private float dca;
    private float dcaAcumulado;
    private float valorTotal;
    private boolean retorno;

	private AutoCompleteTextView produtoTextView;
	private EditText quantidadeEditText;
    private EditText descontoEditText;
    private EditText dcaEditText;
    private EditText precoTabelaEditText;
    private EditText precoEditText;
    private EditText valorTotalEditText;
    private Button negativoPositivoButton;
    private ListView listaTabelaListView;
	
	private Button gravar;

	private String mensagem;

	public PedidosItensFragment() {
	}

    @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_pedidos_itens,
				container, false);
		return rootView;

	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		this.CarregaDados(view);
	}

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisible()){
            if(isVisibleToUser){
                this.CarregaDados(this.getView());
            }
        }
    }

   	@Override
	public void onClick(View v) {

		switch (v.getId()) {
            case R.id.Gravar:
                Gravar(v);
                break;
            case R.id.ItemNegativoPositivo:
                AtualizaPercentual();
                break;
		}

	}

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (v.getId() == R.id.ItemQuantidade) {
            if (!hasFocus) {
                AtualizaQuantidade();
            }
        } else if (v.getId() == R.id.ItemDesconto) {
            if (!hasFocus) {
                AtualizaDesconto();
            }
        } else if (v.getId() == R.id.ItemDca) {
            if (!hasFocus) {
                AtualizaDca();
            }
        }

    }

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long rowId) {
		
		nome = "";

        if (pedido_id == 0){
            pedido_id = ((PedidosActivity)getActivity()).get_pedidoId();
        }

		if (pedido_id == 0) {
			Toast.makeText(context, R.string.pedidos_erro_capa, Toast.LENGTH_LONG).show();
			PedidosCapa();
		}else {
            nome = (String) parent.getItemAtPosition(position);
		}

		ListarPedidoItem(nome);
		
	}

	private void CarregaDados(View view) {

		Bundle args = getArguments();
        if (pedido_id == 0){
            pedido_id = ((PedidosActivity)getActivity()).get_pedidoId();
        }
		
		context = view.getContext();
		controller = new PedidoItemController(context);
		pedidoController = new PedidoController(context);
		produtoController = new ProdutoController(context);
		tabelaController = new TabelaPrecoController(context);
		condicaoController = new CondicaoPagamentoController(context);

        produtoTextView = (AutoCompleteTextView) view.findViewById(R.id.ProdutoProdutoID);
        produtoTextView.setOnItemClickListener(this);

        quantidadeEditText = (EditText) view.findViewById(R.id.ItemQuantidade);
        quantidadeEditText.setOnFocusChangeListener(this);
        quantidadeEditText.setImeOptions(EditorInfo.IME_ACTION_NEXT);

        descontoEditText = (EditText) view.findViewById(R.id.ItemDesconto);
        descontoEditText.setOnFocusChangeListener(this);
        descontoEditText.setImeOptions(EditorInfo.IME_ACTION_NEXT);

        dcaEditText = (EditText) view.findViewById(R.id.ItemDca);
        dcaEditText.setOnFocusChangeListener(this);
        dcaEditText.setImeOptions(EditorInfo.IME_ACTION_NEXT);

        precoTabelaEditText = (EditText) view.findViewById(R.id.ItemPrecoTabela);
        precoEditText = (EditText) view.findViewById(R.id.ItemPreco);
        valorTotalEditText = (EditText) view.findViewById(R.id.ItemValorTotal);

        listaTabelaListView = (ListView) view.findViewById(R.id.TabelaListView);

		gravar = (Button) view.findViewById(R.id.Gravar);
		gravar.setOnClickListener(this);

        negativoPositivoButton = (Button) view.findViewById(R.id.ItemNegativoPositivo);
        negativoPositivoButton.setOnClickListener(this);

		CarregaItens();
		
		ListarPedido();

	}

    private void CarregaItens() {

        cursorProduto = produtoController.Listar(Util.ConverteLong(0), Util.ConverteLong(0));
        ArrayList<String> array = new ArrayList<String>();
        for (cursorProduto.moveToFirst(); !cursorProduto.isAfterLast(); cursorProduto.moveToNext()) {
            array.add(cursorProduto.getString(cursorProduto.getColumnIndex("Nome")));
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, array);
        produtoTextView.setAdapter(adapter);

        LimpaProduto(false);

    }

	private void ListarPedido() {

		Cursor cursor = this.pedidoController.Listar(pedido_id);

		 if(cursor != null && cursor.moveToFirst()){
             tabelaPrecoID = cursor.getLong(cursor.getColumnIndex("TabelaPrecoID"));
             condicaoPagamentoID = cursor.getLong(cursor.getColumnIndex("CondicaoPagamentoID"));

             if (cursor.getLong(cursor.getColumnIndex("ID")) > 0 ){
                 gravar.setEnabled(false);
                 gravar.setBackgroundColor(getResources().getColor(button_text_color));
             }
			 cursor.close();
		}

	}

    private void ListarTabela() {

        Cursor cursorTabela = tabelaController.Listar(tabelaPrecoID, produtoID, 0);

        final TabelaCursorAdapter adapter = new TabelaCursorAdapter(context, cursorTabela);
        this.listaTabelaListView.setAdapter(adapter);

    }

    private void ListarPedidoItem(String nome) {

        produtoID = Util.ConverteLong(0);
        produto = produtoController.BuscarPorIDNome(nome);

        if (produto.equals(null)) {

            LimpaProduto(false);

        }else{

            produtoID = produto.getID();

            pedidoItem = controller.ProcuraItemNoPedido(pedido_id, produtoID);

            LimpaProduto(true);

            if (pedidoItem != null){
                quantidadeEditText.setText(Util.FormataInteiro(pedidoItem.getQuantidade()));
                precoTabelaEditText.setText(Util.FormataDecimal(pedidoItem.getPrecoTabela()));
                descontoEditText.setText(Util.FormataDecimal(pedidoItem.getDesconto()));
                dcaEditText.setText(Util.FormataDecimal(pedidoItem.getDca()));
                AtualizaQuantidade();
            }else{
                AtualizaPreco();
            }

            quantidadeEditText.requestFocus();

        }

    }

    private void LimpaProduto(boolean enable){

        quantidadeEditText.setText("0,000");
        quantidadeEditText.setEnabled(enable);
        descontoEditText.setText("0,00");
        descontoEditText.setEnabled(enable);
        dcaEditText.setText("0,00");
        dcaEditText.setEnabled(enable);
        precoTabelaEditText.setText("0,00");
        precoEditText.setText("0,00");
        valorTotalEditText.setText("0,00");
        negativoPositivoButton.setEnabled(enable);

        /*
        if (produto != null){
            if (produto.getUtilizaDCA() != 1){
                dcaEditText.setEnabled(false);
            }
        }*/

    }

    private boolean AtualizaQuantidade(){

        //Valida a quantidade da fração
        retorno = true;
        quantidade = Util.ConverteFloat(quantidadeEditText.getText().toString());
        fracao = (produto.getFracao() == 0 ? 1 : produto.getFracao());

        if ((quantidade % fracao) > 0){
            quantidade = (((int)(quantidade / fracao)) * fracao) + fracao;

            Toast.makeText(context, getResources().getString(R.string.itens_erro_fracao)
                    .replace("[produto]", Util.ConverteString(produtoID))
                    .replace("[unidade]", Util.ConverteString(produto.getUnidade()))
                    .replace("[fracao]", Util.FormataDecimal(fracao))
                    .replace("[arredondamento]", Util.FormataDecimal(quantidade)), Toast.LENGTH_SHORT).show();

            retorno = false;
        }

        quantidadeEditText.setText(Util.FormataDecimal(quantidade));

        AtualizaPreco();

        return retorno;
    }

    private void AtualizaPreco(){

        //busca preco de tabela e escalonada
        tabela = tabelaController.Buscar(tabelaPrecoID, produtoID, quantidade);
        precoTabela = tabela.getPreco();

        //adiciona percentual de juros da condiçao de pagamento
        if (condicaoController.RetornaJuros(condicaoPagamentoID) > 0){
            precoTabela = precoTabela * condicaoController.RetornaJuros(condicaoPagamentoID) / 100;
        }

        precoTabelaEditText.setText(Util.FormataDecimal(precoTabela));

        ListarTabela();
        AtualizaValorTotal();

    }

    private boolean AtualizaDesconto(){

        retorno = true ;
        desconto = Util.ConverteFloat(descontoEditText.getText().toString());

        if (negativoPositivoButton.getText().toString().equals("+")){
            if (desconto > tabela.getPercentualAcrescimo()){
                desconto = tabela.getPercentualAcrescimo();
                Toast.makeText(context, getResources().getString(R.string.itens_erro_desconto)
                        .replace("[descontoacrescimo]", "acréscimo")
                        .replace("[produto]", Util.ConverteString(produtoID))
                        .replace("[desconto]", Util.FormataDecimal(tabela.getPercentualAcrescimo())) , Toast.LENGTH_SHORT).show();
                desconto = tabela.getPercentualAcrescimo();

                retorno = false;
            }
        }else {
            if (desconto > tabela.getPercentualDesconto()){
                desconto = tabela.getPercentualDesconto();
                Toast.makeText(context, getResources().getString(R.string.itens_erro_desconto)
                        .replace("[descontoacrescimo]", "desconto")
                        .replace("[produto]", Util.ConverteString(produtoID))
                        .replace("[desconto]", Util.FormataDecimal(tabela.getPercentualDesconto())) , Toast.LENGTH_SHORT).show();
                desconto = tabela.getPercentualDesconto();

                retorno = false;
            }
        }

        descontoEditText.setText(Util.FormataDecimal(desconto));

        AtualizaValorTotal();

        return retorno;

    }

    private void AtualizaPercentual(){

        if (negativoPositivoButton.getText().toString().equals("-")){
            negativoPositivoButton.setText(getResources().getString(R.string.botao_positivo));
        }else {
            negativoPositivoButton.setText(getResources().getString(R.string.botao_negativo));
        }

        AtualizaDesconto();
    }

    private boolean AtualizaDca(){

        retorno = true;
        dca = Util.ConverteFloat(dcaEditText.getText().toString());

        if (dca > 0 && tabela.getPercentualDCA() > 0){

            //Valida o máximo de Dca do produto
            dcaPermitido = Util.ConverteFloat(Util.FormataDecimal((quantidade * precoTabela) * tabela.getPercentualDCA() / 100));

            if (dca > dcaPermitido){
                Toast.makeText(context, getResources().getString(R.string.itens_erro_dca)
                        .replace("[produto]", Util.ConverteString(produtoID))
                        .replace("[dca]", Util.FormataDecimal(dcaPermitido)) , Toast.LENGTH_SHORT).show();
                dca = dcaPermitido;

                retorno = false;
            }

            dcaEditText.setText(Util.FormataDecimal(dca));

        }

        AtualizaValorTotal();

        return retorno;

    }

    private void AtualizaValorTotal(){

        //Calcula o preco, o preco liquido e o valor total
        desconto = Util.ConverteFloat(descontoEditText.getText().toString());
        dca = Util.ConverteFloat(dcaEditText.getText().toString());
        dcaAcumulado = 0;
        preco = precoTabela;

        if (desconto > 0){
            if (negativoPositivoButton.getText().toString().equals("-")){
                preco = precoTabela - (precoTabela * desconto / 100);
            }else{
                preco = precoTabela + (precoTabela * desconto / 100);
            }
        }

        valorTotal = (quantidade * preco) - dca;

        if (produto != null && tabela.getAcumulaDCA() == 1) {
            dcaAcumulado = (preco >= precoTabela ? valorTotal * 3 / 100 : 0);
        }

        precoEditText.setText(Util.FormataDecimal(preco));
        valorTotalEditText.setText(Util.FormataDecimal(valorTotal));
        
    }

    private boolean ValidaCampos(){

        boolean retorno = true;
        mensagem =  "";

        if (quantidadeEditText.isFocused()){
            retorno = AtualizaQuantidade();
        }

        if (descontoEditText.isFocused()){
            retorno = AtualizaDesconto();
        }

        if (dcaEditText.isFocused()){
            retorno = AtualizaDca();
        }

        if (produtoID == 0){
            mensagem = context.getString(R.string.itens_erro_produto);
            retorno = false;
            produtoTextView.setFocusable(true);
        }
        if (quantidade == 0){
            mensagem = context.getString(R.string.itens_erro_quantidade);
            retorno = false;
            quantidadeEditText.setFocusable(true);
        }
        if (preco == 0){
            mensagem = context.getString(R.string.itens_erro_preco);
            retorno = false;
            quantidadeEditText.setFocusable(true);
        }

        if (retorno == false && !mensagem.equals("")){
            Toast.makeText(context, mensagem, Toast.LENGTH_SHORT).show();
        }

        return retorno;

    }

    private void Gravar(View view) {

        mensagem = "";

        if (ValidaCampos()) {

            if (pedidoItem == null) {
                pedidoItem = new PedidoItem();
                pedidoItem.set_id((long) 0);
                pedidoItem.setID((long) 0);
            }
            pedidoItem.setPedido_Id(pedido_id);
            pedidoItem.setProdutoID(produtoID);
            pedidoItem.setQuantidade(quantidade);
            pedidoItem.setPreco(preco);
            pedidoItem.setPrecoTabela(precoTabela);
            pedidoItem.setDesconto(desconto);
            pedidoItem.setDca(dca);
            pedidoItem.setDcaAcumulado(dcaAcumulado);
            pedidoItem.setValorTotal(valorTotal);

            try {
                this.controller.Persistir(pedidoItem);
                mensagem = "Produto gravado com sucesso!";
                LimpaProduto(false);
                produtoTextView.setText("");
            } catch (PedidoException e) {
                e.printStackTrace();
                mensagem = e.getMessage();
            }

        }

        if (!mensagem.equals("")) {
            Toast.makeText(context, mensagem, Toast.LENGTH_LONG).show();
        }

    }

    private void PedidosCapa() {
        ((PedidosActivity)getActivity()).getViewPager().setCurrentItem(0);
    }

}
